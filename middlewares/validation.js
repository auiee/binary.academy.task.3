const {
  isValidName, isValidHealth, isValidAttack, isValidDefence, isValidSource,
} = require('../helpers/validators');

/* Validate input. */
const validateInput = function (req, res, next) {
  if      (!Object.keys(req.body).length)     res.status(422).json({ reason: 'empty request' });   // eslint-disable-line max-len,no-multi-spaces
  else if (!isValidName(req.body.name))       res.status(422).json({ reason: 'invalid name' });    // eslint-disable-line max-len,no-multi-spaces
  else if (!isValidHealth(req.body.health))   res.status(422).json({ reason: 'invalid health' });  // eslint-disable-line max-len,no-multi-spaces
  else if (!isValidAttack(req.body.attack))   res.status(422).json({ reason: 'invalid attack' });  // eslint-disable-line max-len,no-multi-spaces
  else if (!isValidDefence(req.body.defence)) res.status(422).json({ reason: 'invalid defence' }); // eslint-disable-line max-len,no-multi-spaces
  else if (!isValidSource(req.body.source))   res.status(422).json({ reason: 'invalid source' });  // eslint-disable-line max-len,no-multi-spaces
  else next();
};

module.exports = validateInput;
