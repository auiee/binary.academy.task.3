/* Add CORS headers. */
const addCorsHeaders = function (req, res, next) {
  res.set({ 'Access-Control-Allow-Origin': '*' });
  next();
};

module.exports = addCorsHeaders;
