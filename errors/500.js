/* Unidentified error. */
const e500 = function (err, req, res, next) {
  if (err) res.status(500).json({ reason: 'server error' });
  else next();
};

module.exports = e500;
