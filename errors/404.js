/* Route not found. */
const e404 = function (req, res, next) {
  res.status(404).json({ reason: 'not found' });
};

module.exports = e404;
