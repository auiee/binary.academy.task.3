/* Not application/json. */
const e422 = function (err, req, res, next) {
  if (err instanceof SyntaxError) res.status(422).json({ reason: 'bad json' });
  else next(err);
};

module.exports = e422;
