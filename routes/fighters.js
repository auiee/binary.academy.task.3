const {
  createFighter, readFighter, updateFighter, deleteFighter,
} = require('../services/fighters');


const express = require('express');
const router = express.Router(); // eslint-disable-line new-cap

/* Create fighter. */
router.post('/', async function (req, res) {
  if (await readFighter(null, req.body.name)) {
    return res.status(409).json({ reason: 'already exists' });
  } else {
    const fighter = await createFighter(req.body);
    res.status(201).json(fighter);
  }
});

/* Read fighter(s). */
router.get('/:id(\\d+)?', async function (req, res) {
  const fighter = await readFighter(req.params.id);

  res.json(fighter);
});

/* Update fighter. */
router.put('/:id(\\d+)', async function (req, res) {
  if (await readFighter(req.params.id)) {
    const fighter = await updateFighter({ _id: req.params.id, ...req.body });
    return res.json(fighter);
  } else {
    return res.status(404).end();
  }
});

/* Delete fighter. */
router.delete('/:id(\\d+)', async function (req, res) {
  if (await readFighter(req.params.id)) await deleteFighter(req.params.id);

  res.status(204).end();
});

module.exports = router;
