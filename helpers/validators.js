/* Validate name. */
const isValidName = name => typeof name === 'string' && /.{2,16}/.test(name);

/* Validate health. */
const isValidHealth = health => _isValidNumberInRange(health, 30, 100);

/* Validate attack. */
const isValidAttack = attack => _isValidNumberInRange(attack, 1, 10);

/* Validate defence. */
const isValidDefence = defence => _isValidNumberInRange(defence, 1, 10);

/* Validate source. */
const isValidSource = function (source) {
  return true;
};

/* Check range intersection. */
const _isValidNumberInRange = function (string, lo, hi) {
  const n = parseFloat(string);
  if (lo <= n && n <= hi) return true;
  else                    return false; // eslint-disable-line no-multi-spaces
};

module.exports = {
  isValidName, isValidHealth, isValidAttack, isValidDefence, isValidSource,
};
