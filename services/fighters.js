const Fighter = require('../models/fighters');

/* Create fighter. */
const createFighter = async function (args) {
  const maxId = await Fighter
    .find()
    .sort('-_id')
    .limit(1)
    .then(x => x[0]._id);
  const fighter = new Fighter({ _id: maxId + 1, ...args });

  fighter.save();
  return fighter;
};

/* Read fighter(s). */
const readFighter = function (_id, name) {
  if (name) return Fighter.find({ name }).then(x => x.length);

  if (!_id) return Fighter.find({}, '_id name source');
  else      return Fighter.findById(_id).then(x => x._doc).catch(e => null); // eslint-disable-line max-len,no-multi-spaces
};

/* Update fighter. */
const updateFighter = async function (args) {
  const fighter = new Fighter({ ...args });

  await Fighter.findByIdAndUpdate(fighter._id, fighter, { useFindAndModify: true }); // eslint-disable-line max-len
  return fighter;
};

/* Delete fighter. */
const deleteFighter = async function (_id) {
  await Fighter.findByIdAndDelete(_id);
};

module.exports = { createFighter, readFighter, updateFighter, deleteFighter };
