const mongoose = require('mongoose');

/* Fighters collection. */
const fighterSchema = new mongoose.Schema({
  _id: Number,
  name: { type: String, unique: true },
  health: Number,
  attack: Number,
  defence: Number,
  source: String,
});

const Fighter = mongoose.model('Fighter', fighterSchema);

module.exports = Fighter;
