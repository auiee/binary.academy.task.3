const validateInput = require('./middlewares/validation');
const addCorsHeaders = require('./middlewares/cors');

const e422 = require('./errors/422');
const e500 = require('./errors/500');
const e404 = require('./errors/404');

const fighters = require('./routes/fighters');

const createDb = require('./models/db');

const express = require('express');

if (process.env.DEBUG) require('dotenv').config();

if (require.main == module) {
  /* Config. */
  const app = express();

  const PORT = process.env.PORT;
  const DB = process.env.DB;

  /* Middleware. */
  app.post('*', express.urlencoded({ extended: true }), validateInput);
  app.put('*', express.urlencoded({ extended: true }), validateInput);
  app.use(addCorsHeaders);

  /* Routes. */
  app.use('/fighters', fighters);

  /* Error handler. */
  app.use(e422, e500, e404);

  /* Run. */
  createDb(DB).then(async () => app.listen(PORT)).catch(e => console.warn(e));
}
